import { Injectable, TemplateRef } from '@angular/core';
import { FormFieldType } from '../../dynamic-form/enums/form-field-type.enum';
import { FormField } from '../../dynamic-form/interfaces/form-field.interface';
import { FormInput } from '../../dynamic-form/interfaces/form-input.interface';
import { Subscribable } from '../../models/components/base-component/subscribable';
import { MatDialog } from '@angular/material/dialog';
import { AddNewQuestionDialogComponent } from '../components/add-new-question-dialog/add-new-question-dialog.component';
import { Subject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class FormService extends Subscribable {
  public readonly formBuilderSpinnerId = 'form_builder_spinner';

  public listNewQuestionItems$ = new Subject();

  public readonly technicalList = ['Angular', 'React', 'C#', 'Other'];

  private listValueBefore: any[] = [];

  constructor(private readonly dialog: MatDialog) {
    super();
  }

  public getFormBuilderFormInput(
    newQuestionTemplate: TemplateRef<any>
  ): FormInput {
    const checkboxFields: FormField[] = this.getCheckBoxListFormField(
      this.technicalList
    );
    const result: FormInput = {
      fields: [
        {
          key: 'about_yourself',
          type: FormFieldType.TEXT_AREA,
          labelOutSide: `Please tell us about yourself *`,
          required: true,
          errors: [
            {
              type: 'required',
              message: `Please input field`,
            }
          ],
        },
        {
          key: 'label',
          type: FormFieldType.LABEL,
          label: `Please select the languages you know *`
        },
        ...checkboxFields,
        {
          key: 'template',
          type: FormFieldType.TEMPLATE_REF,
          templateRef: newQuestionTemplate,
        },
        {
          key: 'add_new',
          type: FormFieldType.LABEL,
          label: `+ Add New Question`,
          linkClicked: () => this.openAddNewQuestionDialog(),
          classes: 'add-new',
        },
      ],
      primaryButtonLabel: `Review my answers >`,
      primaryLocalSpinnerId: this.formBuilderSpinnerId
    };
    return result;
  }

  private getCheckBoxListFormField(list: string[]): FormField[] {
    const result: FormField[] = [];
    list.forEach((x) => {
      const item: FormField = {
        key: x,
        type: FormFieldType.CHECK_BOX,
        label: x,
      };
      result.push(item);
    });
    return result;
  }

  private openAddNewQuestionDialog(): void {
    const dialogRef = this.dialog.open(AddNewQuestionDialogComponent, {});

    dialogRef.afterClosed().subscribe((result) => {
      if (!result) {
        return;
      }
      const data = [...this.listValueBefore, ...result];
      this.listNewQuestionItems$.next(data);
      this.listValueBefore = data;
    });
  }
}
