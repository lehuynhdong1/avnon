import { NewQuestion } from './../../enums/new-question.enum';
import { AppRoutingConstant } from './../../../../constants/app-routing.constant';
import { Component, ViewChild, TemplateRef, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { FormInput } from '../../../dynamic-form/interfaces/form-input.interface';
import { BaseComponent } from '../../../models/components/base-component/base.component';
import { FormService } from '../../services/form.service';
import { LocalSpinnerService } from '../../../local-spinner/services/local-spinner.service';
import { timer } from 'rxjs';

@Component({
  selector: 'my-org-form-builder',
  templateUrl: './form-builder.component.html',
  styleUrls: ['./form-builder.component.scss'],
})
export class FormBuilderComponent extends BaseComponent {
  @ViewChild('newQuestionTemplate', { static: true })
  newQuestionTemplate: TemplateRef<any> | any;

  public formInput: FormInput | any;

  public itemOptions = [];

  constructor(
    private readonly formService: FormService,
    private readonly router: Router,
    private readonly localSpinnerService: LocalSpinnerService
  ) {
    super();
  }

  onSubmit($event: any) {
    const technicalList = this.getTechnicalListValue($event);
    const input = {
      about_yourself: $event.about_yourself,
      technical: technicalList,
    };
    if (technicalList.length === 0) {
      return;
    }

    this.itemOptions
      .filter((x: any) => x.type === NewQuestion.CHECK_BOX)
      .map((y: any) => (y.checkBoxList = this.getCheckBoxListValue(y)));

    this.subscribe(
      this.localSpinnerService.withLocalSpinner(
        timer(2000),
        this.formService.formBuilderSpinnerId
      ),
      () => {
        this.router.navigate(AppRoutingConstant.FORM_ANSWERS, {
          state: {
            static: input,
            dynamic: this.itemOptions,
          },
        });
      }
    );
  }

  protected override onInit(): void {
    this.formInput = this.formService.getFormBuilderFormInput(
      this.newQuestionTemplate
    );

    this.subscribe(this.formService.listNewQuestionItems$, (res: any) => {
      this.itemOptions = res;
      console.log(res);
    });
  }

  private getTechnicalListValue(value: any): string[] {
    const input: string[] = [];
    this.formService.technicalList.forEach((x) => {
      if (value[`${x}`]) {
        input.push(x);
      }
    });
    return input;
  }

  private getCheckBoxListValue(object: any): string[] {
    const input: string[] = [];
    ['answer1', 'answer2', 'answer3', 'answer4'].forEach((x) => {
      if (object[x]?.value) {
        input.push(x);
      }
    });
    return input;
  }
}
