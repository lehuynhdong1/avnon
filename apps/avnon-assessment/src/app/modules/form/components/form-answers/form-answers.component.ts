import { AppRoutingConstant } from './../../../../constants/app-routing.constant';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Router } from '@angular/router';
import { BaseComponent } from '../../../models/components/base-component/base.component';
import { NewQuestion } from '../../enums/new-question.enum';

@Component({
  selector: 'my-org-form-answers',
  templateUrl: './form-answers.component.html',
  styleUrls: ['./form-answers.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FormAnswersComponent extends BaseComponent {
  public static: any;
  public dynamic: any[] = [];

  public NewQuestion = NewQuestion;

  constructor(
    private readonly router: Router,
  ) {
    super();
  }

  public onBack(): void {
    this.router.navigate(AppRoutingConstant.FORM_BUILDER);
  }

  protected override onInit(): void {
    if (history.state?.static) {
      this.static = history.state?.static;
    }

    if (history.state?.dynamic) {
      this.dynamic = history.state?.dynamic;
    }

    if ((this.dynamic.length === 0) && !this.static) {
      this.router.navigate(AppRoutingConstant.FORM_BUILDER);
    }
  }
}
