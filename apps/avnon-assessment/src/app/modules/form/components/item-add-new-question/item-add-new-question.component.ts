import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { BaseComponent } from '../../../models/components/base-component/base.component';
import { NewQuestion } from '../../enums/new-question.enum';

@Component({
  selector: 'my-org-item-add-new-question',
  templateUrl: './item-add-new-question.component.html',
  styleUrls: ['./item-add-new-question.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ItemAddNewQuestionComponent extends BaseComponent {
  @Input() item: any;

  public fields: any;
  public form: FormGroup | any;

  public NewQuestion = NewQuestion;

  constructor(private readonly formBuilder: FormBuilder) {
    super();
  }

  protected override onInit(): void {
    this.form = this.formBuilder.group({
      label: '',
      answer: '',
      answer1: '',
      answer2: '',
      answer3: '',
      answer4: '',
    });

    this.fields = {
      label: {
        placeholder:
          this.item.type === NewQuestion.INPUT
            ? `Please input title question`
            : `Please input label checkbox`,
        required: true,
      },
      answer: {
        placeholder: `Please input answer question`,
        required: true,
      },
      answer1: {
        placeholder: `Please input answer 1 check question`,
        required: true,
      },
      answer2: {
        placeholder: `Please input answer 2 check question`,
        required: true,
      },
      answer3: {
        placeholder: `Please input answer 3 check question`,
        required: true,
      },
      answer4: {
        placeholder: `Please input answer 4 check question`,
        required: true,
      },
    };

    this.initValueChanges();
  }

  private initValueChanges() {
    // input
    ['label', 'answer'].forEach((x) => {
      this.subscribe(this.form.controls[x].valueChanges, (valueChanged) => {
        this.item[x] = valueChanged;
      });
    });

    // checkbox
    ['answer1', 'answer2', 'answer3', 'answer4'].forEach((x) => {
      this.subscribe(this.form.controls[x].valueChanges, (valueChanged) => {
        this.item[x] = {
          label: valueChanged,
          value: false,
        };
      });
    });
  }
}
