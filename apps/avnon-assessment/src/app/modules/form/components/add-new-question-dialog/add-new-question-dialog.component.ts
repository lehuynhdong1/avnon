import { ChangeDetectionStrategy, Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { BaseComponent } from '../../../models/components/base-component/base.component';
import { NewQuestion } from '../../enums/new-question.enum';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'my-org-add-new-question-dialog',
  templateUrl: './add-new-question-dialog.component.html',
  styleUrls: ['./add-new-question-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AddNewQuestionDialogComponent extends BaseComponent {
  public type = '';
  public fields: any;
  public form: FormGroup | any;

  public NewQuestion = NewQuestion;

  public items: any[] = [];

  constructor(
    public dialogRef: MatDialogRef<AddNewQuestionDialogComponent>
  ) {
    super();
  }

  public onQuestionCheckBox(): void {
    this.type = NewQuestion.CHECK_BOX;
    this.items.push({ type: this.type });
  }

  public onQuestionInput(): void {
    this.type = NewQuestion.INPUT;
    this.items.push({ type: this.type });
  }

  public onSubmit(): void {
    this.dialogRef.close(this.items);
  }
}
