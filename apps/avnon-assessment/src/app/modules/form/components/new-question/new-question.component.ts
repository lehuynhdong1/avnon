import { ChangeDetectionStrategy, Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { BaseComponent } from '../../../models/components/base-component/base.component';
import { NewQuestion } from '../../enums/new-question.enum';

@Component({
  selector: 'my-org-new-question',
  templateUrl: './new-question.component.html',
  styleUrls: ['./new-question.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NewQuestionComponent extends BaseComponent implements OnChanges {
  @Input() items: any[] = [];

  public NewQuestion = NewQuestion;

  ngOnChanges(changes: SimpleChanges): void {
    const listValue = changes?.['items']?.currentValue;
    if (listValue.length > 0) {
      this.items = [...listValue];
    }
  }

  public onChange($event: any, index: number): void {
    const name = $event.source.name;
    this.items[index][name] = {
      label: this.items[index][name].label,
      value: $event.checked,
    };
  }
}
