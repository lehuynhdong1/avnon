import { DynamicFormModule } from './../dynamic-form/dynamic-form.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormRoutingModule } from './form-routing.module';
import { FormAnswersComponent } from './components/form-answers/form-answers.component';
import { FormBuilderComponent } from './components/form-builder/form-builder.component';
import { FormComponent } from './components/form.component';
import { AddNewQuestionDialogComponent } from './components/add-new-question-dialog/add-new-question-dialog.component';
import { MatDialogModule } from '@angular/material/dialog';
import { FormService } from './services/form.service';
import { NewQuestionComponent } from './components/new-question/new-question.component';
import { MatMenuModule } from '@angular/material/menu';
import { ItemAddNewQuestionComponent } from './components/item-add-new-question/item-add-new-question.component';
import { MatInputModule } from '@angular/material/input';
import { MatCheckboxModule } from '@angular/material/checkbox';

@NgModule({
  declarations: [
    FormComponent,
    FormAnswersComponent,
    FormBuilderComponent,
    AddNewQuestionDialogComponent,
    NewQuestionComponent,
    ItemAddNewQuestionComponent,
  ],
  imports: [
    CommonModule,
    FormRoutingModule,
    DynamicFormModule,
    MatDialogModule,
    MatMenuModule,
    MatInputModule,
    MatCheckboxModule,
  ],
  providers: [FormService],
})
export class FormModule {}
