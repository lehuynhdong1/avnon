export enum FormFieldInputType {
  TEXT = 'text',
  PASSWORD = 'password',
  NUMBER = 'number',
}
