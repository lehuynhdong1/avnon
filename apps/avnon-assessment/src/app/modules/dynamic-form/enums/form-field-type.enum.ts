export enum FormFieldType {
  ROW,
  INPUT,
  CHECK_BOX,
  TEXT_AREA,
  LABEL,
  TEMPLATE_REF
}
