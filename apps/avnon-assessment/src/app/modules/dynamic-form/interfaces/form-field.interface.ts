import { TemplateRef } from '@angular/core';
import { ValidatorFn } from '@angular/forms';
import { FormFieldInputType } from '../enums/form-field-input-type.enum';
import { FormFieldType } from '../enums/form-field-type.enum';
import { FormObservables } from './form-observables.interface';

export interface FormFieldError {
  // Type can be one of required, email, maxLength...
  type: string;

  // Message to show if error existed
  message: string;
}

export interface BaseFormField extends FormObservables {
  key: string;

  formFieldId?: string;

  type: FormFieldType;

  classes?: string;

  label?: string;

  labelOutSide?: string;

  placeholder?: string;

  validators?: ValidatorFn | ValidatorFn[] | null;

  initValue?: unknown;

  required?: boolean;

  disabled?: boolean;

  hidden?: boolean;

  errors?: FormFieldError[];

  icon?: string;

  /**
   * Use in case of FormFieldType.ROW
   */
  fields?: FormField[];

  valueId?: string;

  /**
   * Templaet Ref
   */
  templateRef?: TemplateRef<any>;
}

export interface InputFormField extends BaseFormField {
  /**
   * Set `input[type]` when type is FormFieldType.INPUT
   */
  inputType?: FormFieldInputType;

  minLength?: number;

  maxLength?: number;

  pattern?: string;

  suffix?: string;

  // extended config for inputType = number
  min?: number;

  max?: number;
}

export interface TextareaFormField extends BaseFormField {
  maxRows?: number;

  minLength?: number;

  maxLength?: number;

  isEditor?: boolean;
}

export interface CheckboxFormField extends BaseFormField {
  labelLink?: string;

  /**
   * Use in case of FormFieldType.CHECKBOX have link in label
   */
  linkClicked?: () => void;
}

export type FormField =
  | BaseFormField
  | InputFormField
  | TextareaFormField
  | CheckboxFormField;
