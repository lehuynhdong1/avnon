import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { BaseComponent } from '../../../models/components/base-component/base.component';
import { CheckboxFormField } from '../../interfaces/form-field.interface';

@Component({
  selector: 'my-org-checkbox',
  templateUrl: './checkbox.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CheckBoxComponent extends BaseComponent {
  @Input() field: CheckboxFormField | any;
  @Input() control: FormControl | any;
}
