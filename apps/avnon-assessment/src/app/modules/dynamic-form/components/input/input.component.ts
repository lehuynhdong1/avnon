import { InputFormField } from '../../interfaces/form-field.interface';
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { BaseComponent } from '../../../models/components/base-component/base.component';

@Component({
  selector: 'my-org-input',
  templateUrl: './input.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InputComponent extends BaseComponent {
  @Input() field: InputFormField | any;
  @Input() control: FormControl | any;
}
