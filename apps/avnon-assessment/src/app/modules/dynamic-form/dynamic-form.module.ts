import { CommonModule } from '@angular/common';
import { NgModule, Type } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormComponent } from '../form/components/form.component';
import { LocalSpinnerModule } from '../local-spinner/local-spinner.module';
import { PipesModule } from '../pipes/pipes.module';
import { AppMaterialModule } from './app-material.module';
import { CheckBoxComponent } from './components/checkbox/checkbox.component';
import { DynamicFormComponent } from './components/form.component';
import { InputComponent } from './components/input/input.component';
import { TextAreaComponent } from './components/text-area/text-area.component';


const components: Type<unknown>[] = [
  DynamicFormComponent,
  InputComponent,
  TextAreaComponent,
  CheckBoxComponent
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    PipesModule,
    AppMaterialModule,
    LocalSpinnerModule,
  ],
  declarations: [components],
  exports: [components],
})
export class DynamicFormModule {}
