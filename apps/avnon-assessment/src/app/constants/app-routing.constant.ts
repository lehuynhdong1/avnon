export class AppRoutingConstant {
  public static readonly FORM = ['form'];
  public static readonly FORM_BUILDER = AppRoutingConstant.FORM.concat('builder');
  public static readonly FORM_ANSWERS = AppRoutingConstant.FORM.concat('answers');
}
