# AVNON INTERVIEW

### Tech stack
- [Nx Workspace][nx]
- [Angular 13][angular]
- [Angular Material][mat]:  UI component and Angular CDK
- [CSS preprocessor][scss]: SCSS

[nx]: https://nx.app/
[angular]: https://angular.io/
[mat]: https://material.angular.io/
[scss]: https://sass-lang.com/guide

### Setting up the development environment 🛠

- `git clone https://gitlab.com/lehuynhdong1/avnon`
- `npm install`
- `npm start` for starting Angular web application
- The app should run on `http://localhost:4200/`

### High-level design
#### Principles

All components are following:
- OnPush Change Detection and async pipes: all components use observable and async pipe for rendering data without any single manual subscribe. Only some places are calling subscribe for dispatching an action.
- SCAMs (single component Angular modules) for tree-shakable components, meaning each component will have a respective module.
- Guide: https://angular.io/guide/cheatsheet

#### Time spending
It's a side project that I just spend my free time working on. I originally planned to finish the project within 4 hours, but the two weekends didn't work out, probably because I had gastrointestinal issues. But when the lego blocks come together, I feel the rhythm, and I know it must be finished by the end of this week.
- TypeScript: 4h
- HTML: 1h
- SCSS: 1h
- Markdown: 30m
- Git: 7m

#### Unit/Integration tests 🧪
I skipped writing test for this project.

